class CreateRecommendations < ActiveRecord::Migration[5.1]
  def change
    create_table :recommendations do |t|
      t.text :description
      t.string :status
      t.text :mission_response
      t.text :comments

      t.timestamps
    end
  end
end
