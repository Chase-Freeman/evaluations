class CreateEvaluations < ActiveRecord::Migration[5.1]
  def change
    create_table :evaluations do |t|
      t.text :name
      t.string :award_type
      t.string :award_number
      t.string :cor
      t.string :contact
      t.date :evaluation_date
      t.string :activity_partner

      t.timestamps
    end
  end
end
