class CreateActions < ActiveRecord::Migration[5.1]
  def change
    create_table :actions do |t|
      t.string :action_type
      t.text :action_description
      t.text :followup
      t.string :action_assignment
      t.date :due_date
      t.string :action_status
      t.date :completion_date

      t.timestamps
    end
  end
end
