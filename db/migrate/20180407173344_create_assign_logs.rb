class CreateAssignLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :assign_logs do |t|
      t.integer :user_id
      t.integer :evaluation_id
      t.integer :recommendation_id
      t.integer :action_id

      t.timestamps
    end
  end
end
