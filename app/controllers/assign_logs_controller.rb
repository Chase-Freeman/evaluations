class AssignLogsController < ApplicationController
  before_action :set_assign_log, only: [:show, :edit, :update, :destroy]

  # GET /assign_logs
  # GET /assign_logs.json
  def index
    @assign_logs = AssignLog.all
  end

  # GET /assign_logs/1
  # GET /assign_logs/1.json
  def show
  end

  # GET /assign_logs/new
  def new
    @assign_log = AssignLog.new
  end

  # GET /assign_logs/1/edit
  def edit
  end

  # POST /assign_logs
  # POST /assign_logs.json
  def create
    @assign_log = AssignLog.new(assign_log_params)

    respond_to do |format|
      if @assign_log.save
        format.html { redirect_to @assign_log, notice: 'Assign log was successfully created.' }
        format.json { render :show, status: :created, location: @assign_log }
      else
        format.html { render :new }
        format.json { render json: @assign_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assign_logs/1
  # PATCH/PUT /assign_logs/1.json
  def update
    respond_to do |format|
      if @assign_log.update(assign_log_params)
        format.html { redirect_to @assign_log, notice: 'Assign log was successfully updated.' }
        format.json { render :show, status: :ok, location: @assign_log }
      else
        format.html { render :edit }
        format.json { render json: @assign_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assign_logs/1
  # DELETE /assign_logs/1.json
  def destroy
    @assign_log.destroy
    respond_to do |format|
      format.html { redirect_to assign_logs_url, notice: 'Assign log was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assign_log
      @assign_log = AssignLog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def assign_log_params
      params.require(:assign_log).permit(:user_id, :evaluation_id, :recommendation_id, :action_id)
    end
end
